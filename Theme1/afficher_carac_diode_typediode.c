#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
	double is; //courant de saturation
	double eta; //coef idealite
	double rs; //resistannce serie
} Diode;

double diode_calculer_tension (Diode diode, double id);

int main(int argc, char *argv[])

{
	if (argc != 7)
		{
			printf("erreur\n");
			return 0;
		}

	Diode diode;

	diode.is = atof(argv[1]);
	diode.rs = atof(argv[2]);
	diode.eta = atof(argv[3]);
	double idi = atof(argv[4]);
	double idf =  atof(argv[5]);
	double pas = atof(argv[6]);
	double id;
	double vd;

	for (id = idi; id<=idf; id=id+pas)
		{
			printf("%lf ",id);
			vd = diode_calculer_tension(diode,id);
			printf("%lf\n",vd);
		}

	return 0;
}

double diode_calculer_tension (Diode diode, double id)
{
        double ut = 1.5; // tension thermodynamique
        return diode.rs*id+diode.eta*ut*log((id/diode.is)+1); // Vd - tension
}

