#include <stdio.h>
#include <stdlib.h>
#include "diode_calculer_tension.c" //importation de la fonction de calcul

int main(int argc, char *argv[])

{
	if (argc != 7)
		{
			printf("erreur\n");
			return 0;
		}

	double is = atof(argv[1]);
	double rs = atof(argv[2]);
	double eta = atof(argv[3]);
	double idi = atof(argv[4]);
	double idf =  atof(argv[5]);
	double pas = atof(argv[6]);
	double id;

	for (id=idi; id<=idf; id=id+pas)
		{
			printf("%lf ",id);
			printf("%lf\n",diode_calculer_tension(is,eta,rs,id));
		}

	return 0;
}


