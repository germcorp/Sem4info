#include <stdio.h>
#include <math.h>

typedef struct {
        double is; //courant de saturation
        double eta; //coef idealite
        double rs; //resistannce serie
} Diode;

double diode_calculer_tension (Diode diode, double id)
{
        double ut = 1.5; // tension thermodynamique
	return diode.rs*id+diode.eta*ut*log((id/diode.is)+1); // Vd - tension
}
