#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])

{
	if (argc != 7) //check arguments
		{
			printf("erreur\n");
			return 0;
		}

	double is = atof(argv[1]); //courant de saturation de la diode
	double rs = atof(argv[2]); //résistance d'accés
	double eta = atof(argv[3]); //coefficient d'idéalité
	double idi = atof(argv[4]); //borne initiale
	double idf =  atof(argv[5]); //borne finale
	double pas = atof(argv[6]); //le pas 
	double id;
	double vd;
	double ut = 1.5; //tension thermodynamique

	for (id = idi; id<=idf; id=id+pas)
		{
			printf("%lf ",id);
			vd = rs*id+eta*ut*log((id/is)+1); //calcul vd
			printf("%lf\n",vd);
		}

	return 0;
}
