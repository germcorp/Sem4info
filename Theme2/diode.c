/* diode.c */

#include "diode.h"

Diode creer_diode(double Is,double eta,double Rs)
{
	Diode diode;
	diode.Is=Is;
	diode.eta=eta;
	diode.Rs=Rs;
	return diode;
}

double diode_calculer_tension (Diode diode, double id)
{
        return diode.Rs*id+diode.eta*UT*log((id/diode.Is)+1); // Vd - tension
}

void enregistrer_carac_diode(double id,double vd, double idi,double idf, double pas,Diode diode,char *filename)
{
	FILE *file;
        file = fopen(filename, "w");

        if (file) {
                for (id = idi; id<=idf; id=id+pas)
                        {
                                printf("%lf ",id);
                                vd = diode_calculer_tension(diode,id);
                                printf("%lf\n",vd);

                                fprintf(file,"%lf ", id);
                                fprintf(file,"%lf\n", vd);
                        }

                fclose(file);
        }
        else
        {
                printf("No filename.\n");
        }
}
/*void demander_et_saisir(Diode *adr_diode)
{
	scanf("%c",&adr_diode->Is);
	scanf("%c",&adr_diode->Rs);
	scanf("%c",&adr_diode->eta);
}
*/
