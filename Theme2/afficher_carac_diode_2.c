#include <stdio.h>
#include <stdlib.h>
#include "diode.h"

int main(int argc, char *argv[])

{
	if (argc != 10)
		{
			printf("Input Error.\n");
			return 0;
		}

	Diode diode;
	Diode diode2;

	diode.Is = atof(argv[1]);
	diode.Rs = atof(argv[2]);
	diode.eta = atof(argv[3]);

	diode2.Is = atof(argv[4]);
        diode2.Rs = atof(argv[5]);
        diode2.eta = atof(argv[6]);

	double idi = atof(argv[7]);
	double idf =  atof(argv[8]);
	double pas = atof(argv[9]);
	double id;

	for (id = idi; id<=idf; id=id+pas)
		{
			printf("%lf ",id);
			printf("%lf ",diode_calculer_tension(diode,id));
			printf("%lf\n",diode_calculer_tension(diode2,id));
		}

	return 0;
}

