/* diode.h */

#ifndef DIODE_H
	#define DIODE_H
	#define UT 1.5
	#include <math.h>
	#include <stdio.h>
	#include <stdlib.h>

	typedef struct {
		double Is; // Courant Saturation
		double eta; // Coefiient idealite
		double Rs; // Resistance serie
	} Diode;

	Diode creer_diode(double Is,double eta,double Rs);
	double diode_calculer_tension(Diode diode,double Id);
	void enregistrer_carac_diode(double id,double vd, double idi,double idf, double pas,Diode diode,char *filename);
	Diode demander_et_saisir(Diode diode);

#endif
