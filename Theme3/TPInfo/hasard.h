/*************************************************
* hasard.h                                       *
* Biblioth�que pour g�n�rer                      *
* des nombre aleatoires                          *
*                                                *
*                                                *
*  F.Marc 2004                                   *
*************************************************/

#ifndef HASARD_H
#define HASARD_H

void initialiser_hasard(void);


int entier_aleatoire(int borne_min, int borne_max);

double reel_aleatoire(double borne_inf, double borne_sup);


#endif /* HASARD_H */


