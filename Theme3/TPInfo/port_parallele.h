
/************************************************
* port_parallele.h                              *
* Biblioth�que pour utiliser le port parall�le  *
*  en mode statique (�criture sans contr�le)    *
*                                               *
*  F.Marc 2004                                  *
************************************************/

#ifndef PORT_PARALLELE_H
#define PORT_PARALLELE_H


#define NOM_PORT_PARALLELE_0 "/dev/parport0"

int port_parallele_ouvrir(const char *nom_port);

void port_parallele_fermer(int fd);

void port_parallele_ecrire (int fd, unsigned char octet);


#endif /* PORT_PARALLELE_H */
