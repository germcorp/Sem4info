/* echelles.h
 Bibliotheque complementaire de Ecran
Affichage des échelles (graduations)

 F.Marc
 septembre 2008
 */

#ifndef ECHELLES_H
#define ECHELLES_H


#include <TPInfo/graphisme.h>

typedef struct  {
	double position_axe; /* position de l'axe dans l'autre dimension */
	double largeur_trait; /* largeur des traits de graduation en pixels */
	double depart;
	double pas; /* espace entre les graduations */
        } Echelle;

typedef struct  {
	Echelle echelle_x;
	Echelle echelle_y;
	int couleur;
        } Echelles;


Echelles creer_echelles_defaut(Ecran *ecran);

void echelles_placer_axe_x_defaut(Echelles *echelles,Ecran *ecran);
void echelles_placer_axe_y_defaut(Echelles *echelles,Ecran *ecran);
void echelles_construire_pas_x_defaut(Echelles *echelles,Ecran *ecran);
void echelles_construire_pas_y_defaut(Echelles *echelles,Ecran *ecran);
void echelle_construire_pas_defaut(Echelle *echelle,double debut, double fin);


void echelles_dessiner(Echelles *echelles,Ecran *ecran);

#endif

