/* echelles.c
 Bibliotheque complementaire de Ecran
Affichage des échelles (graduations)

 F.Marc
 septembre 2008
 */


/* Donne l'exposant de dix immediatement inférieur à |x| */ 

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "echelles.h"

static double puissance_dix(double x,double *mantisse)
{
 double puiss10=1.0;
 double y=fabs(x);
 
 if (y==0.0)
	{
	if (mantisse!=NULL)
		*mantisse=x;
	return 1.0;
 	}

 while (y>=10.0)
	{
	y*=0.1;
	puiss10*=10.0;
	}

 while (y<1.0)
	{
	y*=10.0;
	puiss10*=0.1;
	}

 if (mantisse!=NULL)
	{
	if (x>0.0)
		*mantisse=y;
	else
		*mantisse=-y;
	}
 return puiss10;
}

/* arrondi x à la puissance de dix immediatement inférieure à max_exposant_dix près
exemple : arrondir_decimal(12.456, 0.0345)  --> 12.46 */
static double arrondir_decimal(double x, double max_puissance_10, double *puissance_10)
{
 double puiss10;
 double quotient_entier;
 
 puiss10=puissance_dix(max_puissance_10,NULL);
 if (puissance_10!=NULL)
	*puissance_10=puiss10;

 quotient_entier=floor(x/puiss10);

 return quotient_entier*puiss10;
}


Echelles creer_echelles_defaut(Ecran *ecran)
{
 Echelles echelles;
 echelles.echelle_x.largeur_trait=(ecran->ymax-ecran->ymin)/100;
 echelles.echelle_y.largeur_trait=(ecran->xmax-ecran->xmin)/100;

 echelles_placer_axe_x_defaut(&echelles,ecran);
 echelles_placer_axe_y_defaut(&echelles,ecran);
 echelles_construire_pas_x_defaut(&echelles,ecran);
 echelles_construire_pas_y_defaut(&echelles,ecran);

 echelles.couleur=makecol(255,255,255); 
 return echelles;
}

void echelles_placer_axe_x_defaut(Echelles *echelles,Ecran *ecran)
{
 if (ecran->xmin>0)
	echelles->echelle_x.position_axe= 0.9*ecran->xmin + 0.1 *ecran->xmax;
 else if (ecran->xmax<0)
	echelles->echelle_x.position_axe= 0.1*ecran->xmin + 0.9 *ecran->xmax;
 else 
	echelles->echelle_x.position_axe=0.0;
}

void echelles_placer_axe_y_defaut(Echelles *echelles,Ecran *ecran)
{
 if (ecran->ymin>0)
	echelles->echelle_y.position_axe= 0.9*ecran->ymin + 0.1 *ecran->ymax;
 else if (ecran->ymax<0)
	echelles->echelle_y.position_axe= 0.1*ecran->ymin + 0.9 *ecran->ymax;
 else 
	echelles->echelle_y.position_axe=0.0;
}


void echelles_construire_pas_x_defaut(Echelles *echelles,Ecran *ecran)
{
 echelle_construire_pas_defaut(&(echelles->echelle_x),ecran->xmin,ecran->xmax);
}

void echelles_construire_pas_y_defaut(Echelles *echelles,Ecran *ecran)
{
 echelle_construire_pas_defaut(&(echelles->echelle_y),ecran->ymin,ecran->ymax);
}

void echelle_construire_pas_defaut(Echelle *echelle,double debut, double fin)
{
 double pas_grossier=(debut-fin)/3;
 double pas;
 echelle->depart=arrondir_decimal(debut,pas_grossier,&pas);
 echelle->pas=pas; 
}

void echelles_dessiner(Echelles *echelles,Ecran *ecran)
{
 double x,y;
 
 

/* dessin des axes */
ecran_tracer_segment(ecran,
			ecran->xmin, echelles->echelle_x.position_axe,
			ecran->xmax, echelles->echelle_x.position_axe,
    		 	echelles->couleur);
ecran_tracer_segment(ecran,
			echelles->echelle_y.position_axe,ecran->ymin,
			echelles->echelle_y.position_axe,ecran->ymax,
    		 	echelles->couleur);
/* dessin des graduation */

for(x= echelles->echelle_x.depart; x<=ecran->xmax ; x+= echelles->echelle_x.pas)
	{
	ecran_tracer_segment(ecran,
		x, echelles->echelle_x.position_axe - echelles->echelle_x.largeur_trait,
		x, echelles->echelle_x.position_axe + echelles->echelle_x.largeur_trait,
    		echelles->couleur);
	textprintf_centre_ex(screen, font, x_ecran(ecran,x), y_ecran(ecran,echelles->echelle_x.position_axe- echelles->echelle_x.largeur_trait)+10,
                           echelles->couleur, -1,
                           "%lg",
                           x);
	}
for(y= echelles->echelle_y.depart; y <= ecran->ymax ; y+= echelles->echelle_y.pas)
	{
 	ecran_tracer_segment(ecran,
		echelles->echelle_y.position_axe - echelles->echelle_y.largeur_trait,y,
		echelles->echelle_y.position_axe + echelles->echelle_y.largeur_trait,y,
    		echelles->couleur);
	textprintf_ex(screen, font, x_ecran(ecran,echelles->echelle_y.position_axe + echelles->echelle_y.largeur_trait)+10, y_ecran(ecran,y)-4,
                           echelles->couleur, -1,
                           "%lg",
                           y);

	}

}


