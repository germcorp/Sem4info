/*************************************************
* hasard.c                                       *
* Biblioth�que pour g�n�rer                      *
* des nombre aleatoires                          *
*                                                *
*                                                *
*  F.Marc 2004                                   *
*************************************************/

#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "hasard.h"


void initialiser_hasard(void)
{
srand(time(NULL));
/* c'est une mauvaise methode mais tant pis */
}



int entier_aleatoire(int borne_min, int borne_max)
{
 return (int)floor(reel_aleatoire(borne_min,borne_max+1.0));
}

double reel_aleatoire(double borne_inf, double borne_sup)
{
 return borne_inf+(borne_sup-borne_inf)*(double)rand()/(double)RAND_MAX;
}



