/*************************************************
* fichier_population.h                           *
* Bibliothèque pour lire et manipuler les        *
*  fichier de population (TP informatique LST3)	 *
*                                                *
*  H.Debeda-Hickel & F.Marc 2004                 *
*************************************************/



#ifndef LECTURE_FICHIER_POPULATION_H
#define LECTURE_FICHIER_POPULATION_H

#define LTAB 1000
#define LCHAINE 1000
#include "fichier.h"
#include <stdio.h>

 typedef struct Individu {
 	char nom[LCHAINE];
  	char prenom[LCHAINE];
   	char age[LCHAINE];
    	int departement;
	} Individu;

 typedef struct Population {
 	Individu tableau_individu[LTAB];
  	int nbre_individus;
	} Population;


void lecture_fichier_population(char *nomfichier, Population *pop);


#endif
