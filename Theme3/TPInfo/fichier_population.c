/*************************************************
* fichier_population.c                           *
* Bibliothèque pour lire et manipuler les        *
*  fichier de population (TP informatique LST3)	 *
*                                                *
*  H.Debeda-Hickel & F.Marc 2004                 *
*************************************************/

#include "fichier_population.h"
#include "fichier.h"
#include <stdio.h>

int lecture_ligne(FILE *p, Individu *personne)
{
int nbre_valeurs_lues;
nbre_valeurs_lues= fscanf(p,"%s%s%s%d",(*personne).nom, (*personne).prenom,
((*personne).age),&((*personne).departement));

if (nbre_valeurs_lues ==4)
	return 0;
else
	return 1;
}

void lecture_fichier_population(char *nomfichier, Population *pop)
{
FILE *fic;
int i,erreur=0;

fic = ouvrir_fichier(nomfichier,"r");

for(i=0;!erreur && i<LTAB;i++)
{
	erreur=lecture_ligne(fic,&(pop->tableau_individu[i]));
	if (!erreur)
		pop->nbre_individus=i+1;
}

fermer_fichier(fic);
}






