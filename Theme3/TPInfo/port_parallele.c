/************************************************
* port_parallele.c                              *
* Biblioth�que pour utiliser le port parall�le  *
*  en mode statique (�criture sans contr�le)    *
*                                               *
*  F.Marc 2004                                  *
************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ppdev.h>



int port_parallele_ouvrir(const char *nom_port)
{
    int fd;
    fd = open (nom_port, O_RDWR);
    if (fd == -1) {
        perror ("open");
	exit(1);
	    }
 if (ioctl (fd, PPCLAIM)) {
        perror ("PPCLAIM");
        close (fd);
        exit(1);
    }
 return fd;
}

void port_parallele_fermer(int fd)
{
 close(fd);
}


void port_parallele_ecrire (int fd, unsigned char octet)
{
 ioctl (fd, PPWDATA, &octet);
}

