
/*************************************************
* graphisme.c                                    *
* Biblioth�que pour ouvrir une fen�tre graphique *
*  et faire quelques dessins dedans		 *
*  Cette version utilise la biblioth�que Allegro *
*                                                *
*  F.Marc 2004                                   *
*************************************************/

/* compilation
    compilation s�par�e :
	gcc graphisme.c -c `allegro-config --cflags`
	gcc xxx.o yyy.o graphisme.o -o nom_exec `allegro-config --libs` -Wall

   compilation collective :
	gcc xxx.c  yyy.c graphisme.c -o nom_exec `allegro-config --cflags --libs` -Wall

*/

#include <stdio.h>
#include <stdlib.h>
#include <allegro.h>
#include "graphisme.h"


/**************************************************
* Ouverture et fermeture d'une fen�tre graphique  *
**************************************************/

void ouvrir_fenetre_graphique(int largeur,int hauteur)
{
/* demarrage de la biblioth�que Allegro*/
if (allegro_init())
	{
	fprintf(stderr,"impossible de demarrer la biblioth�que graphique\n");
	exit(1);
	}
/* demarrage du mode graphique fenetr�*/
if (set_gfx_mode(GFX_AUTODETECT_WINDOWED,largeur,hauteur,0,0)!=0)
	{
	fprintf(stderr,"impossible de d'ouvrir la fenetre graphique\n");
	fprintf(stderr,"%s\n",allegro_error);
	exit(1);
	}
/* si on est ici alors le mode graphique a d�marr� */
}

void fermer_fenetre_graphique(void)
{
 set_gfx_mode(GFX_TEXT,0,0,0,0);

}


/***************************************************
* Fonctions associ�es au type  Ecran               *
* qui stocke un systeme de coordonnes              *
* pour pouvoir tracer dans un systeme de           *
* coordonnees diff�rent de celui de l'ecran        *
***************************************************/


Ecran creer_ecran(double xmin, double xmax, double ymin, double ymax)
{
Ecran ecran;
ecran.xmin=xmin;
ecran.xmax=xmax;
ecran.ymin=ymin;
ecran.ymax=ymax;
ecran.a_x=SCREEN_W/(ecran.xmax-ecran.xmin);
ecran.a_y=SCREEN_H/(ecran.ymax-ecran.ymin);

return ecran;
}

int x_ecran(const Ecran *ecran,double x)
{
return (x - ecran->xmin) * ecran->a_x;
}

int y_ecran(const Ecran *ecran,double y)
{
return (ecran->ymax - y) * ecran->a_y;
}

/* tracer un point sur l'Ecran */
void ecran_tracer_point(const Ecran *ecran, double x, double y, int couleur)
{
 putpixel(screen, x_ecran(ecran,x), y_ecran(ecran,y), couleur);
}

/* tracer un segment sur l'Ecran */
void ecran_tracer_segment(const Ecran *ecran,
		 double x1, double y1, 
		 double x2, double y2,
		 int couleur)
{
 int x1_ecran,y1_ecran,x2_ecran,y2_ecran;

x1_ecran = x_ecran(ecran,x1);
y1_ecran = y_ecran(ecran,y1);
x2_ecran = x_ecran(ecran,x2);
y2_ecran = y_ecran(ecran,y2);

line(screen,x1_ecran,y1_ecran,x2_ecran,y2_ecran,couleur);
}

/* tracer une ellipse sur l'Ecran */
void ecran_tracer_ellipse(const Ecran *ecran,
		 double centre_x, double centre_y,
		 double rayon_x,double rayon_y,
		 int couleur )
{
 ellipse(screen, x_ecran(ecran,centre_x), y_ecran(ecran,centre_y),
	rayon_x*ecran->a_x, rayon_y*ecran->a_y, couleur);
}
