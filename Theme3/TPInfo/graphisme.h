
/*************************************************
* graphisme.h                                    *
* Biblioth�que pour ouvrir une fen�tre graphique *
*  et faire quelques dessins dedans		 *
*  Cette version utilise la biblioth�que Allegro *
*                                                *
*  F.Marc 2004                                   *
*************************************************/


/* compilation de la "bilioth�que"
    compilation s�par�e :
	gcc graphisme.c -c `allegro-config --cflags` -Wall
	gcc xxx.o  yyy.o graphisme.o `allegro-config --libs`
	
	o� xxx.c et yyy.c sont les autres fichiers sources � compiler par
	gcc xxx.c -c `allegro-config --cflags` -Wall

   compilation collective :
	gcc xxx.c  yyy.c graphisme.c `allegro-config --cflags --libs` -Wall


APRES la fonction main(), ajouter la ligne :
    END_OF_MAIN();

*/



#ifndef GRAPHISME_H
#define GRAPHISME_H

#include <allegro.h>

/**************************************************
* Ouverture et fermeture d'une fen�tre graphique  *
**************************************************/
void ouvrir_fenetre_graphique(int largeur,int hauteur);
void fermer_fenetre_graphique(void);


/***************************************************
* Type  Ecran                                      *
* stocke un systeme de coordonnes                  *
* pour pouvoir tracer dans un systeme de           *
* coordonnees diff�rent de celui de                *
* l'ecran physique                                 *
***************************************************/
typedef struct Ecran {
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double a_x;
	double a_y;
} Ecran;

/* cr�ation d'un Ecran  d�crit par les nouvelles coordonnes
*(xmin,ymin,xmax,ymax) des bords de la fen�tre*/
/* cette fonction ne cr�e pas la fenetre */
Ecran creer_ecran(double xmin, double xmax, double ymin, double ymax);

/* conversion des coordonnees Ecran vers coordonnes fenetre */
int x_ecran(const Ecran *ecran,double x);
int y_ecran(const Ecran *ecran,double y);


/* tracer un point sur l'Ecran */
void ecran_tracer_point(const Ecran *ecran,double x, double y, int couleur);

/* tracer un segment sur l'Ecran */

void ecran_tracer_segment(const Ecran *ecran,
		 double x1, double y1,
		 double x2, double y2,
		 int couleur);

/* tracer une ellipse sur l'Ecran */
void ecran_tracer_ellipse(const Ecran *ecran,
		 double centre_x, double centre_y,
		 double rayon_x,double rayon_y,
		 int couleur );

#endif /*GRAPHISME_H*/
