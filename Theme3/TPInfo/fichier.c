/*************************************************
* fichier.c                                      *
* Bibliothèque ouvrir et fermer des fichier      *
* en securite                                    *
*                                                *
*  H.Debeda-Hickel & F.Marc 2004                 *
*************************************************/

/* ouverture sécurisée de fichier */ 

#include "fichier.h"
#include <stdlib.h>

FILE *ouvrir_fichier(char *nom, char *mode)
{
	FILE *fichier ;
	fichier= fopen(nom, mode);
	if (fichier==NULL)
		{
		fprintf(stderr,"impossible d'ouvrir %s en mode %s\n", nom, mode);
		exit(1);
			}
	return(fichier);
}

/* fermeture de fichier */

void fermer_fichier(FILE *fichier)
{ 
	fclose(fichier);
}


