/*************************************************
* fichier.h                                      *
* Bibliothèque ouvrir et fermer des fichier      *
* en securite                                    *
*                                                *
*  H.Debeda-Hickel & F.Marc 2004                 *
*************************************************/

/* ouverture sécurisée de fichier */ 

#ifndef FICHIERS_H
#define FICHIERS_H

#include <stdio.h>

FILE *ouvrir_fichier(char *nom, char *mode);


/* fermeture de fichier */

void fermer_fichier(FILE *fichier); 

#endif

