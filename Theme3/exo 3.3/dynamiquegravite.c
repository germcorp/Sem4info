/* dynamiquegravité.c
 Calcul de trajectoire par la 
 méthode d'Euler + energie cinetique; potentiel, effet pesanteur
version 0 (inertie)
 ecrit le 14/02/2018
par : Contreras et Douay
groupe : IMA 401 A11
*/

#include <stdio.h>
#include <stdlib.h> 
//#include <math.h>
#include "vecteurs.h"
//#include <TPInfo/graphisme.h>
//#include <TPInfo/echelles.h>

#define g 9.81
#define mu 1.12
#define GM 2250.
#define M 1


Vecteur nouvelle_position(Vecteur position, Vecteur vitesse, double dt);
Vecteur vitesse_new(Vecteur vitesse, Vecteur pesanteur, double dt);


int main (void) 
{
	char nom_fichier[100];
	FILE *fichier;
	printf("donner le nom du fichier de sortie suivit de .txt:\n");
	scanf("%s",nom_fichier);
	// essai d'ouverture de fichier en mode ecriture
	fichier=fopen(nom_fichier,"w");
	if(fichier==NULL)
	       {
		    printf("Impossible d'ouvrir %s en mode écriture\n" ,nom_fichier);
		    exit(1);
	       } 
	 
	double alpha;
	Vecteur r,v,pesanteur;
	Vecteur r_new;
	Vecteur v_new;
	double x0,y0,v0,t,dt,Ec,Epp,Em;

	/* Initialisation */
		 
	x0 = -10.0; 
	y0 = 0.0;
	t = 0.0;
	dt = 0.05;
	v0 = 15.0;
		 
	printf("angle alpha (en degrés) ?\n");
	scanf("%lf",&alpha);
	alpha = alpha*M_PI/180.0;
	
	r=vecteur_creer(x0,y0); 
	v=vecteur_creer_polaire(v0,alpha);
	pesanteur = vecteur_creer(0.0,-g);
		 
	fprintf(fichier,"x\t y\t Ec\t Epp\t Em\t t\n");

	while(r.y>=0. || t==0.)
		{
			fprintf(fichier,"%lf\t %lf\t",r.x,r.y);
			Ec= 0.5*M*vecteur_module_carre(v); // energie cinétique
			Epp= M*g*r.y; // enrgie potentiel
			Em= Ec+Epp;
			fprintf(fichier,"%lf\t %lf\t %lf\t",Ec,Epp,Em);
			r_new=nouvelle_position(r,v,dt);
			v_new = vitesse_new(v,pesanteur,dt);
			r=r_new;
			v=v_new;
			t = t + dt;
			fprintf(fichier,"%lf\n",t);
		}


	fprintf(fichier,"temps final = %lf s\n",t);

	fclose(fichier);/*fermeture de fichier*/
	printf("fichier enregistré \n");
	
	return 0;
}


Vecteur nouvelle_position(Vecteur position, Vecteur vitesse, double dt) 
{
	Vecteur position_new;
	position_new=vecteur_ajouter(position,vecteur_mult_par_scalaire(dt,vitesse));
 	return position_new;
}

Vecteur vitesse_new(Vecteur vitesse, Vecteur pesanteur, double dt) 
{
	Vecteur vitesse_new;
	vitesse_new=vecteur_ajouter(vitesse,vecteur_mult_par_scalaire(dt,pesanteur));
 	return vitesse_new;
}

//END_OF_MAIN();



