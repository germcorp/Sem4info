#include <stdio.h>
#include <stdlib.h>
#include "vecteurs.h"

Vecteur vecteur_creer(double a, double b)
{
	Vecteur c;
	c.x = a;
	c.y = b;
	return c;
}

Vecteur vecteur_creer_polaire(double a, double b)
{
        Vecteur c;
        c.x = a;
        c.y = b;
        return c;
}

Vecteur vecteur_ajouter(Vecteur a,Vecteur b)
{
	Vecteur c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	return c;
}

double vecteur_produit_scalaire(Vecteur a, Vecteur b)
{
	return a.x*b.x+a.y*b.y;
}

Vecteur vecteur_mult_par_scalaire(double h, Vecteur a)
{
	Vecteur c;
	c.x = a.x*h;
	c.y = a.y*h;
	return c;
}

double vecteur_module(Vecteur a)
{
	return sqrt(a.x*a.x+a.y*a.y);
}

double vecteur_module_carre(Vecteur a)
{
	return sqrt(a.x*a.x+a.y*a.y)*sqrt(a.x*a.x+a.y*a.y);
}

void vecteur_afficher(Vecteur a)
{
	printf("(");
	printf("%lf",a.x);
	printf("%lf",a.y);
	printf(")");
}
