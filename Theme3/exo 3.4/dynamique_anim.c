/* dynamique_anim.c
Calcul de trajectoire par la 
méthode d'Euler + energie cinetique; potentiel, effet pesanteur
version 0 (inertie) + graphique
ecrit le 28/02/2018
par : Lepan Jean-Philippe et Pion Antoine
groupe : IMA 401 A11
*/

#include <stdio.h>
#include <stdlib.h> 
#include <math.h>
#include "vecteur.h"
#include <TPInfo/graphisme.h>
#include <TPInfo/echelles.h>
#include <unistd.h>

#define g 9.81
#define mu 1.12
#define GM 2250.
#define M 1

Vecteur nouvelle_position(Vecteur position, Vecteur vitesse, double dt) 
{
	Vecteur position_new;
	position_new=vecteur_ajouter(position,vecteur_mult_par_scalaire(dt,vitesse));
	return position_new;
}

Vecteur nouvelle_vitesse(Vecteur vitesse, Vecteur pesanteur, double dt) 
{
	Vecteur vitesse_new;
	vitesse_new=vecteur_ajouter(vitesse,vecteur_mult_par_scalaire(dt,pesanteur));
	return vitesse_new;
}

 
int main (void) 
{
	/*char nom_fichier[100];
	FILE *fichier;
	printf("donner le nom du fichier de sortie suivit de .txt:\n");
	scanf("%s",nom_fichier);
	// essai d'ouverture de fichier en mode ecriture
	fichier=fopen(nom_fichier,"w");
	if(fichier==NULL)
	       {
		    printf("Impossible d'ouvrir %s en mode écriture\n" ,nom_fichier);
		    exit(1);
	       }*/
	 
	int hauteur,largeur;
	Ecran mon_ecran;
  	Echelles mes_echelles;
  	int rouge;
	double alpha;
	Vecteur r,v,pesanteur;
	Vecteur r_new,v_new;
	double x0,y0,v0,t,dt;//Ec,Epp,Em;

	/* Initialisation */
		 
	x0 = 0.0; 
	y0 = 0.0;
	t = 0.0;
	dt = 0.05;
	v0 = 15.0;
	hauteur=800;
  	largeur=1000; 
	
	ouvrir_fenetre_graphique(largeur,hauteur);/* creation d'un Ecran, c'est à dire un objet 	pour convertir les coordonnées perso en coordonnées fenetre (pixels) */
	
	mon_ecran=creer_ecran(-10.0,50.0,-10.,100.);
	rouge=makecol(255,50,0);
	
	mes_echelles=creer_echelles_defaut(&mon_ecran);
  	echelles_dessiner(&mes_echelles,&mon_ecran);
	
	printf("angle alpha (en degrés) ?\n");
	scanf("%lf",&alpha);
	alpha = alpha*M_PI/180.0;
	
	r=vecteur_creer(x0,y0); 
	v=vecteur_creer(v0*cos(alpha),v0*sin(alpha));
	pesanteur=vecteur_creer(0.0,-g);
		 
	//fprintf(fichier,"x\t y\t Ec\t Epp\t Em\t t\n");

	while(r.y>=0. || t==0.)
		{
			/*fprintf(fichier,"%lf\t %lf\t",r.x,r.y);
			Ec= 0.5*M*vecteur_module_carre(v); // energie cinétique
			Epp= M*g*r.y; // energie potentiel
			Em= Ec+Epp;
			fprintf(fichier,"%lf\t %lf\t %lf\t",Ec,Epp,Em);*/
			r_new=nouvelle_position(r,v,dt);
			v_new=nouvelle_vitesse(v,pesanteur,dt);
			r=r_new;
			v=v_new;
			t = t + dt;
			//fprintf(fichier,"%lf\n",t);	
  			ecran_tracer_point(&mon_ecran,r.x,r.y,rouge);
			sleep(1.);
		}
	

	//fprintf(fichier,"temps final = %lf s\n",t);

	//fclose(fichier);/*fermeture de fichier*/
	//printf("fichier enregistré \n");
	
	printf("Appuyez sur <entree> pour terminer le programme\n");
  	scanf("%*c%*c");

  	fermer_fenetre_graphique();

	return 0;
}
END_OF_MAIN();

