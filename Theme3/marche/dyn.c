#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vecteur.h"

#define g 9.81
#define mu 1.12
#define GM 2250.

Vecteur nouvelle_position(Vecteur position, Vecteur vitesse, double dt) {
	Vecteur position_new;
	position_new=vecteur_ajouter(position,vecteur_mult_par_scalaire(dt,vitesse));
 	return position_new;
}


int main (int argc, char *argv[]) {
  double alpha = atof(argv[1]);
  char *filename = argv[2];
  Vecteur r,v;
  Vecteur r_new;
  double x0,y0,v0,t,dt;


  /* Initialisation */

  x0 = -10.;
  y0 = 0;
  t = 0.;
  dt = 0.05;
  v0 = 15.;
  //printf("angle alpha (en degrÃ©s) ?\n");
  //scanf("%lf",&alpha);
  alpha = alpha*M_PI/180.;

  r=vecteur_creer(x0,y0);
  v=vecteur_creer_polaire(v0,alpha);

  FILE *file;
  file = fopen(filename, "w");

	if (file)
	{
	  while(t<5.0)
		{
		printf("%lf %lf\n",r.x,r.y);
		fprintf(file,"%lf %lf\n",r.x,r.y);
		r_new=nouvelle_position(r,v,dt);
  		r=r_new;
		t = t + dt;
		}
		fclose(file);
	}

	printf("temps final = %lf s\n",t);
	
  return 0;
}
