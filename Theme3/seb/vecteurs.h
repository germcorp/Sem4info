/* vecteur.h

Vecteurs
Auteurs: Contreras
Date : 07/02/2018*/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
	double x;
	double y;
	double r;
	double eta;
	
	
} Vecteur;

Vecteur vecteur_creer(double x, double y);
Vecteur vecteur_creer_polaire(double r, double eta);
Vecteur vecteur_ajouter(Vecteur a, Vecteur b);
double vecteur_produit_scalaire(Vecteur a, Vecteur b);
Vecteur vecteur_mult_par_scalaire(double h, Vecteur a);
double vecteur_module_carre(Vecteur a);
double vecteur_module(Vecteur a);
void vecteur_afficher(Vecteur a);