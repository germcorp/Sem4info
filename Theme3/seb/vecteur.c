/* Exercice 3.1
vecteur creer.c
Auteurs:Douay et Contreras
Date:07/02/2018*/ 


#include <stdio.h>
#include <math.h>
#include "vecteurs.h"



Vecteur vecteur_creer(double x, double y)

{
	Vecteur v;
	v.x = x;
	v.y = y;
	return v;
}


/* vecteur creer polaire.c
Auteurs:Douay et Contreras
Date:07/02/2018*/ 


Vecteur vecteur_creer_polaire(double r, double eta)
{
	Vecteur v;
	v.r = r*cos(eta);
	v.eta = r*sin(eta);
	return v;
}
	
	
/* vecteur ajouter.c
Auteurs:Douay et Contreras
Date:07/02/2018*/ 

Vecteur vecteur_ajouter(Vecteur a, Vecteur b)
{
	Vecteur c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	return c;
}

/* vecteur scalaire.c
Auteurs:Douay et Contreras
Date:07/02/2018*/ 

double vecteur_produit_scalaire(Vecteur a, Vecteur b)
{
	return a.x*b.x+a.y*b.y;
}

/* vecteur mutiplication.c
Auteurs:Douay et Contreras
Date:07/02/2018*/ 

Vecteur vecteur_mult_par_scalaire(double h, Vecteur a)
{
	Vecteur c;
	c.x = a.x*h;
	c.y = a.y*h;
	return c;
}


/* vecteur module carre.c
Auteurs:Douay et Contreras
Date:07/02/2018*/ 

double vecteur_module_carre(Vecteur a)
{
	return sqrt(a.x*a.x+a.y*a.y)*sqrt(a.x*a.x+a.y*a.y);
}

/* vecteur module .c
Auteurs:Douay et Contreras
Date:07/02/2018*/ 

double vecteur_module(Vecteur a)
{
	return sqrt(a.x*a.x+a.y*a.y);
}


void vecteur_afficher(Vecteur a)
{
	printf("(");
	printf("%lf",a.x);
	printf("%lf",a.y);
	printf(")");
}


//exécution


/*int main(int argc, char *argv[])

{

	Vecteur v;
	v.r = atof(argv[1]);
	v.eta = atof(argv[2]);
	printf("%lf\n", v.r);
	printf("%lf\n", v.eta);
	return 0;
}

*/